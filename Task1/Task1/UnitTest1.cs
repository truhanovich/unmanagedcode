﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Runtime.InteropServices;

namespace Task1
{

    public struct SYSTEM_BATTERY_STATE
    {
        public byte AcOnLine;
        public byte BatteryPresent;
        public byte Charging;
        public byte Discharging;
        public byte spare1;
        public byte spare2;
        public byte spare3;
        public byte spare4;
        public UInt32 MaxCapacity;
        public UInt32 RemainingCapacity;
        public Int32 Rate;
        public UInt32 EstimatedTime;
        public UInt32 DefaultAlert1;
        public UInt32 DefaultAlert2;
    }

    struct SYSTEM_POWER_INFORMATION
    {
        public ulong MaxIdlenessAllowed;
        public ulong Idleness;
        public ulong TimeRemaining;
        public byte CoolingMode;
    }

    [TestClass]
    public class UnitTest1
    {
        [DllImport("powrprof.dll", SetLastError = true)]
        private static extern uint CallNtPowerInformation(
            int InformationLevel,
            IntPtr lpInputBuffer,
            UInt32 nInputBufferSize,
            IntPtr lpOutputBuffer,
            UInt32 nOutputBufferSize
         );


        [TestMethod]
        public void TestMethod1()
        {
            IntPtr status = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(ulong)));
            CallNtPowerInformation(15, (IntPtr)null, 0, status, (UInt32)Marshal.SizeOf(typeof(ulong)));

            uint statusValue = (uint)Marshal.ReadInt32(status);
            Console.WriteLine("a. LastSleepTime: {0}", statusValue);

            CallNtPowerInformation(14, (IntPtr)null, 0, status, (UInt32)Marshal.SizeOf(typeof(ulong)));
            statusValue = (uint)Marshal.ReadInt32(status);
            Console.WriteLine("b. LastWakeTime: {0}", statusValue);



            int size = Marshal.SizeOf<SYSTEM_BATTERY_STATE>();
            IntPtr batteryStatePtr = Marshal.AllocCoTaskMem(size);
            CallNtPowerInformation(5, IntPtr.Zero, 0, batteryStatePtr, (uint)size);

            SYSTEM_BATTERY_STATE batteryState = Marshal.PtrToStructure<SYSTEM_BATTERY_STATE>(batteryStatePtr);
            Console.WriteLine("Battery level: {0}/{1}", batteryState.RemainingCapacity, batteryState.MaxCapacity);

            size = Marshal.SizeOf<SYSTEM_POWER_INFORMATION>();
            IntPtr systemPowerStatePtr = Marshal.AllocCoTaskMem(size);
            CallNtPowerInformation(5, IntPtr.Zero, 0, systemPowerStatePtr, (uint)size);

            SYSTEM_POWER_INFORMATION powerInfoState = Marshal.PtrToStructure<SYSTEM_POWER_INFORMATION>(systemPowerStatePtr);
            Console.WriteLine("Battery level: {0}", powerInfoState.ToString());

            //2. Резервировать и удалять hibernation файл
            //CallNtPowerInformation(10, (IntPtr)null, 1, status, (UInt32)Marshal.SizeOf(typeof(ulong)));
            //statusValue = (uint)Marshal.ReadInt32(status);
            //Console.WriteLine("b. LastWakeTime: {0}", statusValue);
        }

        [DllImport("Powrprof.dll", SetLastError = true)]
        static extern bool SetSuspendState(bool hibernate, bool forceCritical, bool disableWakeEvent);

        [TestMethod]
        public void TestMethod2()
        {
            SetSuspendState(true, true, true);
            // Standby
            SetSuspendState(false, true, true);
        }
    }
}
