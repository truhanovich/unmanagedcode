﻿using System;
using System.Runtime.InteropServices;

namespace Task2
{
    [ComVisible(true)]
    [Guid("9be61da6-853f-45ef-b4b8-02dec18a028c")]
    [ClassInterface(ClassInterfaceType.None)]
    public class PowerState : IPowerState
    {
        [DllImport("powrprof.dll", SetLastError = true)]
        private static extern uint CallNtPowerInformation(
            int InformationLevel,
            IntPtr lpInputBuffer,
            UInt32 nInputBufferSize,
            IntPtr lpOutputBuffer,
            UInt32 nOutputBufferSize
         );

        [DllImport("Powrprof.dll", SetLastError = true)]
        static extern bool SetSuspendState(bool hibernate, bool forceCritical, bool disableWakeEvent);


        public bool MySetSuspendState(bool hibernate, bool forceCritical, bool disableWakeEvent)
        {
            return SetSuspendState(hibernate, forceCritical, disableWakeEvent);
        }

        public int LastSleapTiem()
        {
            IntPtr status = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(ulong)));
            CallNtPowerInformation(15, (IntPtr)null, 0, status, (UInt32)Marshal.SizeOf(typeof(ulong)));

            return Marshal.ReadInt32(status);
        }

        public int LastWakeUpTime()
        {
            IntPtr status = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(ulong)));
            CallNtPowerInformation(15, (IntPtr)null, 0, status, (UInt32)Marshal.SizeOf(typeof(ulong)));

            CallNtPowerInformation(14, (IntPtr)null, 0, status, (UInt32)Marshal.SizeOf(typeof(ulong)));
            return Marshal.ReadInt32(status);
        }

        public SYSTEM_BATTERY_STATE BatteryState()
        {
            int size = Marshal.SizeOf<SYSTEM_BATTERY_STATE>();
            IntPtr batteryStatePtr = Marshal.AllocCoTaskMem(size);
            CallNtPowerInformation(5, IntPtr.Zero, 0, batteryStatePtr, (uint)size);

            return Marshal.PtrToStructure<SYSTEM_BATTERY_STATE>(batteryStatePtr);
        }

        public SYSTEM_POWER_INFORMATION PowerInformatio()
        {
            int size = Marshal.SizeOf<SYSTEM_POWER_INFORMATION>();
            IntPtr systemPowerStatePtr = Marshal.AllocCoTaskMem(size);
            CallNtPowerInformation(5, IntPtr.Zero, 0, systemPowerStatePtr, (uint)size);

            return Marshal.PtrToStructure<SYSTEM_POWER_INFORMATION>(systemPowerStatePtr);
        }

        public void WorkWihtHibernation(int isDel)
        {
            IntPtr status = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(ulong)));
            CallNtPowerInformation(10, (IntPtr)null, (uint)isDel, status, (UInt32)Marshal.SizeOf(typeof(ulong)));
        }
    }
}
