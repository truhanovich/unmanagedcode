﻿using System;
using System.Runtime.InteropServices;

namespace Task2
{
    [ComVisible(true)]
    [Guid("72573b46-f62e-4605-bbd1-485552ae1d22")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IPowerState
    {
        bool MySetSuspendState(bool hibernate, bool forceCritical, bool disableWakeEvent);
        int LastSleapTiem();
        int LastWakeUpTime();
        SYSTEM_BATTERY_STATE BatteryState();
        SYSTEM_POWER_INFORMATION PowerInformatio();
        void WorkWihtHibernation(int isDel);
    }
}
